const turf = require('@turf/turf')
const GeoJSON = require('geojson')
const jsonfile = require('jsonfile')
const cp = require('copy-paste')

/**
 * UNUSED, not working.
 * Use this to verify that the grid is as big as it should be.
 * It will read the featureCollection from file and merge it in into one polygon
 * and then copy-paste the GeoJSON output to the clipboard
 * @param  {[type]} fileName [description]
 * @return {[type]}          [description]
 */
const readToEnvelope = function (fileName) {
  jsonfile.readFile(fileName, function(err, obj) {
    if (err) throw err
    console.log(obj.features.length)
    const superPolygon = turf.envelope(obj)
    cp.copy(JSON.stringify(superPolygon))
  })
}('./grid.json')

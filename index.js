const generator = require('./generator')
const cp = require('copy-paste')

// Defined bounds in WGS84, these are the swedish ones.
const bounds = {
  nw: {
    lat: 69.1279892,
    lng: 6.9801503
  },
  ne: {
    lat: 68.6877388,
    lng: 29.5717853
  },
  sw: {
    lat: 54.7182805,
    lng: 24.1149108
  },
  se: {
    lat: 55.4738053,
    lng: 9.9653922
  }
}

/**
 * Callback function will return a complete featureCollection containing the the entire grid for the given bounds
 * and square-size
 * @param  {[type]} results [description]
 * @return {[type]}         [description]
 */
generator(bounds, 50000, function (results) {
  // Copy the stringified output, ready for "testing"
  cp.copy(JSON.stringify(results))
})

## Node.JS Grid Generator

## How to use
Run `npm install`  
Adjust values inside `index.js` and then just run the file with node.

### Generate
Run `index.js` to generate a very large featureCollection. It will copy to clipboard once its done
so don't go crazy and crash your computer. Adjust values inside index.js to get a more sane result.

### Issues
When attempting to create a 100m grid, it seems to slow down around 6million features (when writing files)
See `envelope.js` for details regarding an attempt to save this data to file.

### Import (in theory)
* Spin up a mongodb instance.
* Stack overflow suggests:
  * use `jq` (sed-like program for JSON)
  * Adjust the GeoJSON (because mongodb will assume that the GeoJSON is one document, and not a document per feature)
  * `jq --compact-output ".features" input.geojson > output.geojson`
    * Should turn the single array into a large array of features.
  * Import JSON into mongo through `mongoimport`
    * `mongoimport --db dbname -c collectionname --file "output.geojson" --jsonArray`
  * Additional performance related flags can be used
    * `-j` - number of workers
    * `--batchSize=x` amount per batch insert?
* After inserting data
  * Create a 2dsphere index.
  * `db.collectionname.createIndex({ geometry : "2dsphere" } )`
  * Play with geospatial queries

#### Example MongoDB query
```js
db.collectionname.find(
  { loc :
    { $geoWithin :
      { $geometry :
        { type : "Polygon",
          coordinates : [
            [[0 , 0], [0, 1], [ 1, 1], [1, 0], [0, 0]]
          ]
        }
      }
    }
  }
 )
```

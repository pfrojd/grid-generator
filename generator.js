const GeoJSON = require('geojson')
const destination = require('@turf/destination')
const envelope = require('@turf/envelope')
const helpers = require('@turf/helpers')

/**
 * A "enum" for picking out a correct coordinate pair
 * @type {Object}
 */
const DIRECTION = {
  TOPRIGHT: 2,
  BOTTOMLEFT: 4
}

/**
 * Convert a LatLng Object to GeoJSON
 * @param  {Object} latlng [A plain object with lat and lng properties]
 * @return {Object}        [A GeoJSON Point Feature]
 */
const latLngToGeoJSON = function (latlng) {
  return GeoJSON.parse(latlng, { Point: ['lat', 'lng'] })
}

/**
 * Get the coordinate pair in a given direction from a feature
 * @param  {Object} feature   [A GeoJSON Feature]
 * @param  {string} direction [A value from the DIRECTION enum]
 * @return {Number[]}         [A coordinate pair]
 */
const getNextCoordinates = function (feature, direction) {
  return feature.geometry.coordinates[0][DIRECTION[direction]]
}

/**
 * Generate a new polygon feature from a given NorthWest GeoJSON point
 * and a size.
 * @param  {Object} nw     [A GeoJSON Point to start Polygon from]
 * @param  {Number} radius        [Size of the polygon in meters]
 * @return {Object}      [A GeoJSON feature containing the polygon]
 */
const generatePolygonFeature = function (nw, radius) {
  // destination will calculate a point given a starting point, a "radius" (which is just a distance),
  // a bearing and a defined unit of measurement.
  const ne = destination(nw, radius, 90, 'metres')
  const se = destination(ne, radius, 180, 'metres')
  const sw = destination(se, radius, -90, 'metres')

  // Given the four points of the square, turn them into a featureCollection
  const fc = helpers.featureCollection([
    nw, ne, se, sw
  ])

  // Turn that featureCollectiong into a single feature
  return envelope(fc)
}

/**
 * Generate a grid of polygons given a set of bounds and size
 * @param  {Object} bounds [An object containing bounds]
 * @param  {Number} radius [Length of the polygon]
 * @param  {Function} callback [Callback]
 * @return {Object}        [A GeoJSON FeatureCollection]
 */
const generator = function (bounds, radius, callback) {
  const results = []
  let keepGoing = true
  let amount = 1
  let rows = 0

  // The first created feature is created from the latlng bounds object.
  // Convert it to GeoJSON first, and pass it to the function.
  let lastFeature = generatePolygonFeature(latLngToGeoJSON({
    lat: bounds.nw.lat,
    lng: bounds.nw.lng
  }), radius)

  // Save the feature as the first in row.
  let firstInRow = lastFeature

  // Create a feature collection with the first feature.
  // const featureCollection = turf.featureCollection([lastFeature])
  results.push(lastFeature)

  // Time to create the remaining grid.
  while (keepGoing) {
    // Create a point feature from the TOPRIGHT coordinate pair of the last feature
    const nextPoint = helpers.point(getNextCoordinates(lastFeature, 'TOPRIGHT'))

    // Given the point, create a new polygon feature.
    const feature = generatePolygonFeature(nextPoint, radius)

    // Add it to the feature collection
    results.push(feature)

    // Set the newly created feature as the last created.
    lastFeature = feature

    // Get the TOPRIGHT coordinate pair for the last created feature.
    let northEast = getNextCoordinates(lastFeature, 'TOPRIGHT')[0]

    // If the coordinate pair is beyond the bounds for the TOPRIGHT corner, a row is finished.
    if (northEast >= bounds.ne.lng) {
      rows++
      console.log('Finished row ' + rows)
      // Check if the BOTTOMLEFT coordinate pair of the first feature in the row
      // is beyond the BOTTOMLEFT bounds.
      if (getNextCoordinates(firstInRow, 'BOTTOMLEFT')[1] <= bounds.se.lat) {
        callback(helpers.featureCollection(results))

        // Stop the while-loop
        keepGoing = false
      }

      // Row is finished, but we're not done with the grid, which means we need to create
      // the first feature in the new row. Create it using the first feature in the previous
      // rows BOTTOMLEFT coordinate pair.
      const nextLineFeature = generatePolygonFeature(helpers.point(getNextCoordinates(firstInRow, 'BOTTOMLEFT')), radius)

      // Add it to the collection.
      results.push(nextLineFeature)

      // Save features as needed and continue the loop.
      lastFeature = nextLineFeature
      firstInRow = nextLineFeature
    }
    amount++
    console.log('Finished feature ' + amount)
  }
}

module.exports = generator
